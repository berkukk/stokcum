STOK TAKİP PROGRAMI
Gereksinim Analizi
-Müşteri İstekleri
•	Sahip olduğu işyerlerinin stoğunu takip eden program olsun.
•	Şirket sahibinin yetkisinin mağazalara stok işlemesi olsun.
•	Diğer kullanıcıların ise stokları takip edilmesi

-Yazılım Gereksinimleri
•	SQL Veritabanı(SQL Management Studio)
•	Microsoft Visual Studio(C#)

-Sistem Girdisi
	Ürünlerin Listesi
	Mağaza Çalışanlarının Listesi ve Bilgileri
	Mağaza Sahibinin ve Çalışanların Yetkileri(Veritabanı Yetkisi)
-Sistem Çıktısı
	Ürünlerin Çeşidine Göre Gruplandırılması
	Satılan Mağaza Ürünleri
	Mağazadaki Ürünlerin Stok Durumu

-Sistem Gereksinimleri
•	Sunucu
	Yeterli Bellek Alanı
	Veritabanı için Yeterli Hafıza Alanı
•	Programın çalışacağı ortamlar
	Çalışacağı Bilgisayarlar
	Bilgisayarların donanımı

-Kaynak
	Masaüstü Uygulaması
	Sunucuya Bağlı Veritabanı

                                                                           Proje Adı:Stokcum
                                                                           Hazırlayanlar:Ahmet KILIÇ – 294992
        																			Ali Emre DEMİRCİ – 295014
        																	   Muhammet Berkuk KANŞAT- 295019

