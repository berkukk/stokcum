﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AnaEkran : Form
    {
        public AnaEkran()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void stokSorgulamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sorgu nesne = new Sorgu();
            nesne.Show();
        }

        private void satışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Satis nesne = new Satis();
            nesne.Show();
        }

        private void müşterilerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Müşteri nesne = new Müşteri();
            nesne.Show();
        }

        private void faturalarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fatura nesne = new Fatura();
            nesne.Show();
        }

        private void çalışanlarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ŞÇalışan nesne = new ŞÇalışan();
            nesne.Show();

        }

        private void şubelerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ŞŞubeler nesne = new ŞŞubeler();
            nesne.Show();
        }
    }
}
