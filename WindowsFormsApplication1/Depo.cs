﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Depo : Form
    {
        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public Depo()
        {
            InitializeComponent();
        }

        private void Depo_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Ürünler' table. You can move, or remove it, as needed.
            //this.ürünlerTableAdapter.Fill(this.stokçuDataBaseDataSet.Ürünler);
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Ürünler' table. You can move, or remove it, as needed.
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select * from Ürünler", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataTable ds = new DataTable();
            dt.Fill(ds);
            dataGridView1.DataSource = ds;
            baglanti.Close();
            

        }

        private void Urun_Ekle()
        {
            baglanti.Open();
            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "" && textBox5.Text == "" && textBox6.Text == "" && textBox7.Text == "" && textBox8.Text == "")
            {
                MessageBox.Show("Hatalı Giriş");
            }
            SqlCommand komut = new SqlCommand("insert into Ürünler(idŞube,idÜrün,ÜrünMarkası,AyakkabıNumarası,Fiyat,AyakkabıRengi,AyakkabıCinsiyeti,ÜrünStok) values (@1,@2,@3,@4,@5,@6,@7,@8)", baglanti);
            komut.Parameters.AddWithValue("@1", textBox1.Text);
            komut.Parameters.AddWithValue("@2", textBox2.Text);
            komut.Parameters.AddWithValue("@3", textBox3.Text);
            komut.Parameters.AddWithValue("@4", textBox4.Text);
            komut.Parameters.AddWithValue("@5", textBox5.Text);
            komut.Parameters.AddWithValue("@6", textBox6.Text);
            komut.Parameters.AddWithValue("@7", textBox7.Text);
            komut.Parameters.AddWithValue("@8", textBox8.Text);
            komut.ExecuteNonQuery();
            goster();
            baglanti.Close();
        }

        private void Urun_Sil()
        {
            baglanti.Open();
            DataGridViewRow select = new DataGridViewRow();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    SqlCommand komut = new SqlCommand("Delete from Ürünler where idÜrün='" + dataGridView1.Rows[i].Cells[1].Value + "' AND idŞube='" + dataGridView1.Rows[i].Cells[8].Value + "' AND AyakkabıNumarası='" + dataGridView1.Rows[i].Cells[3].Value + "'", baglanti);
                    komut.ExecuteNonQuery();
                    MessageBox.Show("Silindi.");
                    break;
                }
            }
            baglanti.Close();
            goster();
        }

        private void Urun_Guncelle()
        {
            baglanti.Open();
            DataGridViewRow select = new DataGridViewRow();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    SqlCommand komut = new SqlCommand("UPDATE Ürünler set idŞube='" + textBox1.Text + "',idÜrün='" + textBox2.Text + "',ÜrünMarkası='" + textBox3.Text + "',AyakkabıNumarası='" + textBox4.Text + "',Fiyat='" + textBox5.Text + "',AyakkabıRengi='" + textBox6.Text + "',AyakkabıCinsiyeti='" + textBox7.Text + "',ÜrünStok='" + textBox8.Text + "' where idÜrün='" + dataGridView1.Rows[i].Cells[1].Value + "' AND idŞube='" +dataGridView1.Rows[i].Cells[8].Value + "' AND AyakkabıNumarası='" +dataGridView1.Rows[i].Cells[3].Value + "'", baglanti);
                    komut.ExecuteNonQuery();
                    dataGridView1.Rows[i].Cells[8].Value = Convert.ToString(textBox1.Text);
                    dataGridView1.Rows[i].Cells[1].Value = Convert.ToString(textBox2.Text);
                    dataGridView1.Rows[i].Cells[2].Value = Convert.ToString(textBox3.Text);
                    dataGridView1.Rows[i].Cells[3].Value = Convert.ToString(textBox4.Text);
                    dataGridView1.Rows[i].Cells[4].Value = Convert.ToString(textBox5.Text);
                    dataGridView1.Rows[i].Cells[5].Value = Convert.ToString(textBox6.Text);
                    dataGridView1.Rows[i].Cells[6].Value = Convert.ToString(textBox7.Text);
                    dataGridView1.Rows[i].Cells[7].Value = Convert.ToString(textBox8.Text);
                    MessageBox.Show("Güncellendi.");
                    break;
                }
            }
            baglanti.Close();
            goster();
        }
        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select *from Ürünler where idÜrün like '%" + textBox9.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();


        }



        private void goster()
        {
            
            SqlCommand komut = new SqlCommand("Select *from Ürünler", baglanti);
            SqlDataAdapter ad = new SqlDataAdapter(komut);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            dataGridView1.DataSource = dt;
            
        }

private void button2_Click(object sender, EventArgs e)
{
    Urun_Ekle();
}

private void button3_Click(object sender, EventArgs e)
{
   Urun_Sil();
}

private void button1_Click(object sender, EventArgs e)
{
    Urun_Guncelle();
}

private void textBox9_TextChanged(object sender, EventArgs e)
{
    Sorgula();
}

private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
{
    
        textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
        textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
        textBox7.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
        textBox8.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString();
        
    
}

private void button4_Click(object sender, EventArgs e)
{
    textBox1.Text = "";
    textBox2.Text = "";
    textBox3.Text = "";
    textBox4.Text = "";
    textBox5.Text = "";
    textBox6.Text = "";
    textBox7.Text = "";
    textBox8.Text = "";
    
}

        }



    }

