﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Müşteri : Form
    {
        string s = Giris.gonder;
        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public Müşteri()
        {
            InitializeComponent();
        }

        private void Müşteri_Load(object sender, EventArgs e)
        {
            
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Müşteri' table. You can move, or remove it, as needed.
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Müşteri.MüşteriİsimSoyisim,Müşteri.Cinsiyet,Müşteri.MüşteriTelefon,Fatura.idÜrün from Müşteri INNER JOIN Fatura ON Müşteri.MüşteriNo=Fatura.MüşteriNo where Fatura.ŞNo = " + s, baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataTable ds = new DataTable();
            dt.Fill(ds);
            dataGridView1.DataSource = ds;
            baglanti.Close();

        }
        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Müşteri.MüşteriİsimSoyisim,Müşteri.Cinsiyet,Müşteri.MüşteriTelefon,Fatura.idÜrün from Müşteri INNER JOIN Fatura ON Müşteri.MüşteriNo=Fatura.MüşteriNo where Fatura.ŞNo = '" + s + "' AND Müşteri.MüşteriİsimSoyisim like '%" + textBox1.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { Müşteri_Load(sender, e); }
            else
            {
                Sorgula();
            }
        }
    }
}
