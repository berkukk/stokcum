﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using PagedList;

namespace WindowsFormsApplication1
{
    public partial class Fatura : Form
    {
        string g=Giris.gonder;
        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public Fatura()
        {
            InitializeComponent();
        }

        private void Fatura_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Fatura' table. You can move, or remove it, as needed.
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Fatura.FNo,Müşteri.MüşteriİsimSoyisim,Fatura.FTarihi,Fatura.NFiyat,Fatura.İndirim,Fatura.TFiyat,Fatura.idÜrün from Fatura INNER JOIN Müşteri ON Fatura.MüşteriNo=Müşteri.MüşteriNo where Fatura.ŞNo= "+g, baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataTable ds = new DataTable();
            dt.Fill(ds);
            dataGridView1.DataSource = ds;
            baglanti.Close();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Fatura.FNo,Müşteri.MüşteriİsimSoyisim,Fatura.FTarihi,Fatura.NFiyat,Fatura.İndirim,Fatura.TFiyat,Fatura.idÜrün from Fatura INNER JOIN Müşteri ON Fatura.MüşteriNo=Müşteri.MüşteriNo where Fatura.ŞNo= '" + g + "' AND Müşteri.MüşteriİsimSoyisim like '%" + textBox1.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "") { Fatura_Load(sender, e); }
            else { Sorgula(); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pageSetupDialog1.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult pd = printDialog1.ShowDialog();
            if (pd == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }
        string ürün = "";
        string şube = "";

        private void urunsube_cek()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Şubeİsim from Şube where idŞube=" + g, baglanti);
            SqlDataReader dr = komut.ExecuteReader();
            if (dr.Read()) { şube = dr["Şubeİsim"].ToString(); }
            baglanti.Close();
            baglanti.Open();
            DataGridViewRow select = new DataGridViewRow();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    SqlCommand komut1 = new SqlCommand("Select ÜrünMarkası from Ürünler where idÜrün=" + select.Cells[7].Value, baglanti);
                    SqlDataReader dr1 = komut1.ExecuteReader();
                    if (dr1.Read()) { ürün = dr1["ÜrünMarkası"].ToString(); }
                }
            }
            baglanti.Close();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            DataGridViewRow select = new DataGridViewRow();
            Font myFont = new Font("Calibri", 28);
            SolidBrush sbrush = new SolidBrush(Color.Black);
            SolidBrush cizgi = new SolidBrush(Color.Red);
            Pen myPen = new Pen(Color.Black);
            Pen cizgi1 = new Pen(Color.Red);

            e.Graphics.DrawImage(Properties.Resources.Stock_take, 100, 20);

            e.Graphics.DrawLine(cizgi1, 120, 120, 750, 120);
            e.Graphics.DrawLine(cizgi1, 120, 180, 750, 180);
            e.Graphics.DrawString("Ürünler", myFont, cizgi, 250, 120);

            e.Graphics.DrawLine(myPen, 50, 320, 800, 320);


            myFont = new Font("Calibri", 7, FontStyle.Bold);
            e.Graphics.DrawString("Fatura Numarası", myFont, sbrush, 75, 328);
            e.Graphics.DrawString("Alınan Ürün", myFont, sbrush, 175, 328);
            e.Graphics.DrawString("Alınan Şube", myFont, sbrush, 250, 328);
            e.Graphics.DrawString("Tarih", myFont, sbrush, 325, 328);
            e.Graphics.DrawString("Müşteri", myFont, sbrush, 425, 328);
            e.Graphics.DrawString("Fiyat", myFont, sbrush, 525, 328);
            e.Graphics.DrawString("İndirim", myFont, sbrush, 600, 328);
            e.Graphics.DrawString("Toplam Fiyat", myFont, sbrush, 650, 328);

            e.Graphics.DrawLine(myPen, 50, 348, 800, 348);

            int y = 360;
            urunsube_cek();
            StringFormat myStringFormat = new StringFormat();
            myStringFormat.Alignment = StringAlignment.Far;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    e.Graphics.DrawString(select.Cells[1].Value.ToString(), myFont, sbrush, 100, y, myStringFormat);
                    e.Graphics.DrawString(ürün, myFont, sbrush, 225, y, myStringFormat);
                    e.Graphics.DrawString(şube, myFont, sbrush, 285, y, myStringFormat);
                    e.Graphics.DrawString(select.Cells[3].Value.ToString(), myFont, sbrush, 375, y, myStringFormat);
                    e.Graphics.DrawString(select.Cells[2].Value.ToString(), myFont, sbrush, 475, y, myStringFormat);
                    e.Graphics.DrawString(select.Cells[4].Value.ToString(), myFont, sbrush, 575, y, myStringFormat);
                    e.Graphics.DrawString(select.Cells[5].Value.ToString(), myFont, sbrush, 650, y, myStringFormat);
                    e.Graphics.DrawString(select.Cells[6].Value.ToString(), myFont, sbrush, 700, y, myStringFormat);
                }
            }



        }
    }
}
