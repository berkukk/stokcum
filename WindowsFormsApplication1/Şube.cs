﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Şube : Form
    {

        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public Şube()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void Şube_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Şube' table. You can move, or remove it, as needed.
            this.şubeTableAdapter.Fill(this.stokçuDataBaseDataSet.Şube);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Sube_Ekle();
        }
        private void Sube_Sil()
        {
            baglanti.Open();
            DataGridViewRow select = new DataGridViewRow();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    SqlCommand komut = new SqlCommand("Delete from Şube where idŞube=(" + dataGridView1.Rows[i].Cells[1].Value + ")", baglanti);
                    komut.ExecuteNonQuery();
                    MessageBox.Show("Silindi.");
                    break;
                }
            }
            baglanti.Close();
            goster();

        }
        private void Sube_Ekle()
        {
            baglanti.Open();
            if (textBox1.Text == "" && textBox3.Text == "" && textBox2.Text == "") 
                MessageBox.Show("Hatalı Giriş");

            SqlCommand komut = new SqlCommand("insert into Şube(Şubeİsim,ŞŞifre,ŞTelefon) values (@1,@2,@3)", baglanti);
            komut.Parameters.AddWithValue("@1", textBox1.Text);
            komut.Parameters.AddWithValue("@2", textBox2.Text);
            komut.Parameters.AddWithValue("@3", textBox3.Text);
            komut.ExecuteNonQuery();
            goster();

            baglanti.Close();
            

        }

        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select *from Şube where Şubeİsim like '%" + textBox4.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void Guncelle()
        {
            baglanti.Open();
            DataGridViewRow select = new DataGridViewRow();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                select = dataGridView1.Rows[i];
                if (Convert.ToBoolean(select.Cells[0].Value) == true)
                {
                    SqlCommand komut = new SqlCommand("UPDATE Şube set Şubeİsim='" + textBox1.Text + "',ŞŞifre='" + textBox2.Text + "',ŞTelefon='" + textBox3.Text + "' where idŞube=" + dataGridView1.Rows[i].Cells[1].Value, baglanti);
                    komut.ExecuteNonQuery();
                    dataGridView1.Rows[i].Cells[2].Value = Convert.ToString(textBox1.Text);
                    dataGridView1.Rows[i].Cells[3].Value = Convert.ToString(textBox2.Text);
                    dataGridView1.Rows[i].Cells[4].Value = Convert.ToString(textBox3.Text);
                    MessageBox.Show("Güncellendi.");
                    break;
                }
            }
            baglanti.Close();
            goster();

        }

        private void goster()
        {
            SqlCommand komut = new SqlCommand("Select *from Şube", baglanti);
            SqlDataAdapter ad = new SqlDataAdapter(komut);
            DataTable dt=new DataTable();
            ad.Fill(dt);
            dataGridView1.DataSource=dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Sube_Sil();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            Sorgula();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Guncelle();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Yönetici nesne = new Yönetici();
            nesne.Show();
            this.Hide();
        }
    }
}
