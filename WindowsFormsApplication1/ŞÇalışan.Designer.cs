﻿namespace WindowsFormsApplication1
{
    partial class ŞÇalışan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idÇalışanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.çisimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.çyaşDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.çtelefonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.çalışanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stokçuDataBaseDataSet = new WindowsFormsApplication1.StokçuDataBaseDataSet();
            this.çalışanTableAdapter = new WindowsFormsApplication1.StokçuDataBaseDataSetTableAdapters.ÇalışanTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.çalışanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stokçuDataBaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idÇalışanDataGridViewTextBoxColumn,
            this.çisimDataGridViewTextBoxColumn,
            this.çyaşDataGridViewTextBoxColumn,
            this.çtelefonDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.çalışanBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(51, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1232, 548);
            this.dataGridView1.TabIndex = 0;
            // 
            // idÇalışanDataGridViewTextBoxColumn
            // 
            this.idÇalışanDataGridViewTextBoxColumn.DataPropertyName = "idÇalışan";
            this.idÇalışanDataGridViewTextBoxColumn.HeaderText = "idÇalışan";
            this.idÇalışanDataGridViewTextBoxColumn.Name = "idÇalışanDataGridViewTextBoxColumn";
            this.idÇalışanDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // çisimDataGridViewTextBoxColumn
            // 
            this.çisimDataGridViewTextBoxColumn.DataPropertyName = "Çisim";
            this.çisimDataGridViewTextBoxColumn.HeaderText = "Çisim";
            this.çisimDataGridViewTextBoxColumn.Name = "çisimDataGridViewTextBoxColumn";
            // 
            // çyaşDataGridViewTextBoxColumn
            // 
            this.çyaşDataGridViewTextBoxColumn.DataPropertyName = "çyaş";
            this.çyaşDataGridViewTextBoxColumn.HeaderText = "çyaş";
            this.çyaşDataGridViewTextBoxColumn.Name = "çyaşDataGridViewTextBoxColumn";
            // 
            // çtelefonDataGridViewTextBoxColumn
            // 
            this.çtelefonDataGridViewTextBoxColumn.DataPropertyName = "çtelefon";
            this.çtelefonDataGridViewTextBoxColumn.HeaderText = "çtelefon";
            this.çtelefonDataGridViewTextBoxColumn.Name = "çtelefonDataGridViewTextBoxColumn";
            // 
            // çalışanBindingSource
            // 
            this.çalışanBindingSource.DataMember = "Çalışan";
            this.çalışanBindingSource.DataSource = this.stokçuDataBaseDataSet;
            // 
            // stokçuDataBaseDataSet
            // 
            this.stokçuDataBaseDataSet.DataSetName = "StokçuDataBaseDataSet";
            this.stokçuDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // çalışanTableAdapter
            // 
            this.çalışanTableAdapter.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(509, 624);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(382, 34);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(365, 627);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sorgu";
            // 
            // ŞÇalışan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.Stock_take;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1344, 1045);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Times New Roman", 13.8F);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ŞÇalışan";
            this.Text = "Stokçum v2.0";
            this.Load += new System.EventHandler(this.ŞÇalışan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.çalışanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stokçuDataBaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private StokçuDataBaseDataSet stokçuDataBaseDataSet;
        private System.Windows.Forms.BindingSource çalışanBindingSource;
        private StokçuDataBaseDataSetTableAdapters.ÇalışanTableAdapter çalışanTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idÇalışanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn çisimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn çyaşDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn çtelefonDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}