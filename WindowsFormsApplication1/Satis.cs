﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Satis : Form
    {
        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public Satis()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Satis_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Ürünler' table. You can move, or remove it, as needed.
            baglanti.Open();
            string ad = Giris.gonder;
            SqlCommand komut = new SqlCommand("Select * from Ürünler where idŞube=" + ad, baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataTable ds = new DataTable();
            dt.Fill(ds);
            dataGridView1.DataSource = ds;
            baglanti.Close();
        }
        private void goster()
        {
            baglanti.Open();
            string da = Giris.gonder;
            SqlCommand komut = new SqlCommand("Select * from Ürünler where idŞube=" + da, baglanti);
            SqlDataAdapter ad = new SqlDataAdapter(komut);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            dataGridView1.DataSource = dt;
            baglanti.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SatisBut();
        }

        private void SatisBut()
        {
            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "") { MessageBox.Show("Bilgileri Doldurunuz."); }
            else
            {
                
                int stoksayi = 0;
                int ürün = 0;
                baglanti.Open();
                DataGridViewRow select = new DataGridViewRow();
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    select = dataGridView1.Rows[i];
                    if (Convert.ToBoolean(select.Cells[0].Value) == true)
                    {
                        SqlCommand komut3 = new SqlCommand("Select ÜrünStok,idÜrün from Ürünler where idÜrün='" + dataGridView1.Rows[i].Cells[1].Value + "' AND idŞube='" + dataGridView1.Rows[i].Cells[8].Value + "' AND AyakkabıNumarası='" + dataGridView1.Rows[i].Cells[3].Value + "'", baglanti);
                        SqlDataReader dr1 = komut3.ExecuteReader();
                        if (dr1.Read())
                        {
                            stoksayi = Convert.ToInt32(dr1["ÜrünStok"]);
                            ürün = Convert.ToInt32(dr1["idÜrün"]);


                        }
                    }
                }
                baglanti.Close();

                stoksayi -= Convert.ToInt32(textBox2.Text);

                if (stoksayi < 0)
                {
                    MessageBox.Show("Yeterli Stok Yok!");

                }
                else
                {
                    baglanti.Open();

                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        select = dataGridView1.Rows[i];
                        if (Convert.ToBoolean(select.Cells[0].Value) == true)
                        {
                            SqlCommand komut4 = new SqlCommand("UPDATE Ürünler set ÜrünStok='" + stoksayi + "' where idÜrün='" + dataGridView1.Rows[i].Cells[1].Value + "' AND idŞube='" + dataGridView1.Rows[i].Cells[8].Value + "' AND AyakkabıNumarası='" + dataGridView1.Rows[i].Cells[3].Value + "'", baglanti);
                            komut4.ExecuteNonQuery();
                            MessageBox.Show("Satıldı.");
                            break;
                        }
                    }
                    baglanti.Close();
                    int mno = 1;
                    baglanti.Open();
                    SqlCommand komut = new SqlCommand("Select MüşteriNo from Müşteri", baglanti);
                    SqlDataReader dr = komut.ExecuteReader();
                    while (dr.Read())
                    {
                        mno = Convert.ToInt32(dr["MüşteriNo"]);
                        mno++;
                    }

                    baglanti.Close();
                    baglanti.Open();
                    SqlCommand komut2 = new SqlCommand("insert into Müşteri(MüşteriNo,MüşteriİsimSoyisim,Cinsiyet,MüşteriTelefon) values (@1,@2,@3,@4)", baglanti);
                    komut2.Parameters.AddWithValue("@1", mno);
                    komut2.Parameters.AddWithValue("@2", textBox1.Text);
                    komut2.Parameters.AddWithValue("@3", comboBox1.Text);
                    komut2.Parameters.AddWithValue("@4", textBox3.Text);
                    komut2.ExecuteNonQuery();
                    baglanti.Close();
                    int fno = 1;
                    baglanti.Open();
                    SqlCommand komut6 = new SqlCommand("Select FNo from Fatura", baglanti);
                    SqlDataReader dr2 = komut6.ExecuteReader();
                    while (dr2.Read())
                    {
                        fno = Convert.ToInt32(dr2["FNo"]);
                        fno++;
                    }
                    int fiyat=0;
                    int ifiyat = 0;
                    baglanti.Close();
                    baglanti.Open();
                  for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    select = dataGridView1.Rows[i];
                    if (Convert.ToBoolean(select.Cells[0].Value) == true)
                    {
                        SqlCommand komut3 = new SqlCommand("Select Fiyat from Ürünler where idÜrün='" + dataGridView1.Rows[i].Cells[1].Value + "' AND idŞube='" + dataGridView1.Rows[i].Cells[8].Value + "' AND AyakkabıNumarası='" + dataGridView1.Rows[i].Cells[3].Value + "'", baglanti);
                        SqlDataReader dr1 = komut3.ExecuteReader();
                        if (dr1.Read())
                        {
                            fiyat = Convert.ToInt32(dr1["Fiyat"]);

                        }
                    }
                }
                  baglanti.Close();
                  fiyat = fiyat * Convert.ToInt32(textBox2.Text);
                  if (textBox5.Text == "") { ifiyat = fiyat; }
                    else{
                    ifiyat=fiyat-((fiyat*Convert.ToInt32(textBox5.Text))/100);}
                    string dd=Giris.gonder;
                    baglanti.Open();
                    SqlCommand komut5 = new SqlCommand("insert into Fatura(FNo,FTarihi,TFiyat,ŞNo,idÜrün,İndirim,MüşteriNo,NFiyat) values (@1,@2,@3,@4,@5,@6,@7,@8)", baglanti);
                    komut5.Parameters.AddWithValue("@1", fno);
                    komut5.Parameters.AddWithValue("@2", DateTime.Now);
                    komut5.Parameters.AddWithValue("@3", ifiyat);
                    komut5.Parameters.AddWithValue("@4", dd);
                    komut5.Parameters.AddWithValue("@5", ürün);
                    komut5.Parameters.AddWithValue("@6", textBox5.Text);
                    komut5.Parameters.AddWithValue("@7", mno);
                    komut5.Parameters.AddWithValue("@8", fiyat);
                    komut5.ExecuteNonQuery();
                    baglanti.Close();
                    goster();
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    comboBox1.Text = "";
                }
                }
                
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select * from Ürünler where idÜrün like '%" + textBox4.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();


        }
    }
}
