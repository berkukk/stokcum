﻿namespace WindowsFormsApplication1
{
    partial class Şube
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Şube));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idŞubeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.şubeİsimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.şŞifreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.şTelefonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.şubeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stokçuDataBaseDataSet = new WindowsFormsApplication1.StokçuDataBaseDataSet();
            this.şubeTableAdapter = new WindowsFormsApplication1.StokçuDataBaseDataSetTableAdapters.ŞubeTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.şubeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stokçuDataBaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.idŞubeDataGridViewTextBoxColumn,
            this.şubeİsimDataGridViewTextBoxColumn,
            this.şŞifreDataGridViewTextBoxColumn,
            this.şTelefonDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.şubeBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(78, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(761, 496);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Seç";
            this.Column1.Name = "Column1";
            // 
            // idŞubeDataGridViewTextBoxColumn
            // 
            this.idŞubeDataGridViewTextBoxColumn.DataPropertyName = "idŞube";
            this.idŞubeDataGridViewTextBoxColumn.HeaderText = "idŞube";
            this.idŞubeDataGridViewTextBoxColumn.Name = "idŞubeDataGridViewTextBoxColumn";
            this.idŞubeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // şubeİsimDataGridViewTextBoxColumn
            // 
            this.şubeİsimDataGridViewTextBoxColumn.DataPropertyName = "Şubeİsim";
            this.şubeİsimDataGridViewTextBoxColumn.HeaderText = "Şubeİsim";
            this.şubeİsimDataGridViewTextBoxColumn.Name = "şubeİsimDataGridViewTextBoxColumn";
            // 
            // şŞifreDataGridViewTextBoxColumn
            // 
            this.şŞifreDataGridViewTextBoxColumn.DataPropertyName = "ŞŞifre";
            this.şŞifreDataGridViewTextBoxColumn.HeaderText = "ŞŞifre";
            this.şŞifreDataGridViewTextBoxColumn.Name = "şŞifreDataGridViewTextBoxColumn";
            // 
            // şTelefonDataGridViewTextBoxColumn
            // 
            this.şTelefonDataGridViewTextBoxColumn.DataPropertyName = "ŞTelefon";
            this.şTelefonDataGridViewTextBoxColumn.HeaderText = "ŞTelefon";
            this.şTelefonDataGridViewTextBoxColumn.Name = "şTelefonDataGridViewTextBoxColumn";
            // 
            // şubeBindingSource
            // 
            this.şubeBindingSource.DataMember = "Şube";
            this.şubeBindingSource.DataSource = this.stokçuDataBaseDataSet;
            // 
            // stokçuDataBaseDataSet
            // 
            this.stokçuDataBaseDataSet.DataSetName = "StokçuDataBaseDataSet";
            this.stokçuDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // şubeTableAdapter
            // 
            this.şubeTableAdapter.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1023, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(222, 22);
            this.textBox1.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(1023, 129);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(222, 22);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(1023, 101);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(222, 22);
            this.textBox2.TabIndex = 3;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(864, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Şube İsmi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(845, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "Şube Şifresi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(882, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 22);
            this.label3.TabIndex = 6;
            this.label3.Text = "Telefon";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.Location = new System.Drawing.Point(999, 170);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 63);
            this.button1.TabIndex = 7;
            this.button1.Text = "Şube Ekle";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button2.Location = new System.Drawing.Point(999, 342);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 67);
            this.button2.TabIndex = 8;
            this.button2.Text = "Şube Sil";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(923, 442);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(270, 22);
            this.textBox4.TabIndex = 9;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button3.Location = new System.Drawing.Point(999, 252);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(194, 62);
            this.button3.TabIndex = 10;
            this.button3.Text = "Güncelle";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button4.Location = new System.Drawing.Point(2, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 496);
            this.button4.TabIndex = 11;
            this.button4.Text = "G\r\nE\r\nR\r\nİ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Şube
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1344, 520);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Şube";
            this.Text = "Stokçum v2.0";
            this.Load += new System.EventHandler(this.Şube_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.şubeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stokçuDataBaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private StokçuDataBaseDataSet stokçuDataBaseDataSet;
        private System.Windows.Forms.BindingSource şubeBindingSource;
        private StokçuDataBaseDataSetTableAdapters.ŞubeTableAdapter şubeTableAdapter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idŞubeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn şubeİsimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn şŞifreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn şTelefonDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}