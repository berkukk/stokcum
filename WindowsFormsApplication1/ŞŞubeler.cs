﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class ŞŞubeler : Form
    {
        SqlConnection baglanti = new SqlConnection("Data Source=DESKTOP-UGQ85ES\\Psiblen;Initial Catalog=StokçuDataBase;Integrated Security=True");
        public ŞŞubeler()
        {
            InitializeComponent();
        }

        private void ŞŞubeler_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'stokçuDataBaseDataSet.Şube' table. You can move, or remove it, as needed.
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select idŞube,Şubeİsim,ŞTelefon from Şube", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataTable ds = new DataTable();
            dt.Fill(ds);
            dataGridView1.DataSource = ds;
            baglanti.Close();

        }

        private void Sorgula()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select idŞube,Şubeİsim,ŞTelefon from Şube where Şubeİsim like '%" + textBox1.Text + "%'", baglanti);
            SqlDataAdapter dt = new SqlDataAdapter(komut);
            DataSet ds = new DataSet();
            dt.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            if (textBox1.Text == "") { ŞŞubeler_Load(sender, e); }
            else { Sorgula(); }
        }
    }
}
